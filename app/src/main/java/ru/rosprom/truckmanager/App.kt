package ru.rosprom.truckmanager

import android.app.Application
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.rosprom.truckmanager.api.TrucksApi

class App : Application() {

    companion object {
        const val BASE_URL: String = "http://rsprm.ru/test/"

        lateinit var instance: App
        lateinit var api: TrucksApi
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        initApi()
    }

    fun initApi() {
        api = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(Utils.getGson()))
            .baseUrl(BASE_URL)
            .build()
            .create(TrucksApi::class.java)
    }

}