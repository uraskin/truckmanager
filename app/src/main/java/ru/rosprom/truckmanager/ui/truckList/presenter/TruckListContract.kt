package ru.rosprom.truckmanager.ui.truckList.presenter

import ru.rosprom.truckmanager.data.jsonResponse.Truck


interface TruckListContract {
    fun loadTruckList()

    fun addNewTruck()

    fun editTruck(truck: Truck)

    fun removeTruck(truck: Truck)
}