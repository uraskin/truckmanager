package ru.rosprom.truckmanager.ui.truckList.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ru.rosprom.truckmanager.App
import ru.rosprom.truckmanager.api.MapNetworkCompletableErrors
import ru.rosprom.truckmanager.api.MapNetworkSingleErrors
import ru.rosprom.truckmanager.data.jsonResponse.Truck

@InjectViewState
class TruckListPresenter : MvpPresenter<TruckListView>(), TruckListContract {
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun loadTruckList() {
        viewState.showProgressBar()
        compositeDisposable.add(App.api.getTrucks()
            .compose(MapNetworkSingleErrors())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {trucks: List<Truck>? ->
                    viewState.showTruckList(trucks)
                    viewState.hideProgressBar() },
                {error: Throwable ->
                    viewState.showErrorMessage(error.localizedMessage)
                    viewState.hideProgressBar() }
            )
        )
    }

    override fun addNewTruck() {
        viewState.showNewTruckScreen()
    }

    override fun editTruck(truck: Truck) {
        viewState.showEditTruckScreen(truck)
    }

    override fun removeTruck(truck: Truck) {
        viewState.showProgressBar()
        compositeDisposable.add(App.api.deleteTruck(truck.id)
            .compose(MapNetworkCompletableErrors())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    viewState.onTruckRemoved(truck)
                    viewState.hideProgressBar()
                },
                {error ->
                    viewState.hideProgressBar()
                    viewState.showErrorMessage(error.localizedMessage)
                }))
    }
}