package ru.rosprom.truckmanager.ui.truckDetail

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.widget.doOnTextChanged
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_truck_detail.*
import ru.rosprom.truckmanager.R
import ru.rosprom.truckmanager.Utils
import ru.rosprom.truckmanager.data.jsonResponse.Truck
import ru.rosprom.truckmanager.mvp.MvpAndroidxActivity
import ru.rosprom.truckmanager.ui.truckDetail.presenter.TruckDetailPresenter
import ru.rosprom.truckmanager.ui.truckDetail.presenter.TruckDetailView


class TruckDetailActivity : MvpAndroidxActivity(), TruckDetailView {

    companion object {
        const val ACTION_TYPE: String = "ACTION_TYPE"

        const val TRUCK_ID: String = "TRUCK_ID"
        const val TRUCK_NAME: String = "TRUCK_NAME"
        const val TRUCK_PRICE: String = "TRUCK_PRICE"
        const val TRUCK_COMMENT: String = "TRUCK_COMMENT"

        const val ACTION_TYPE_ADD_TRUCK = 0
        const val ACTION_TYPE_EDIT_TRUCK = 1
    }

    @InjectPresenter
    lateinit var presenter: TruckDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_truck_detail)

        val actionType = intent.getIntExtra(ACTION_TYPE, 0)
        val truckId = intent.getStringExtra(TRUCK_ID)
        val truckName = intent.getStringExtra(TRUCK_NAME)
        val truckPrice = intent.getStringExtra(TRUCK_PRICE)
        val truckComment = intent.getStringExtra(TRUCK_COMMENT)

        initView(actionType, truckName, truckPrice, truckComment)

        setSupportActionBar(toolbarLayout as Toolbar)
        supportActionBar?.title = if (actionType == 0) getString(R.string.new_truck) else getString(R.string.edit_truck)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        saveTruck.setOnClickListener {
            Utils.hideKeyboard(this)
            when (actionType) {
                ACTION_TYPE_ADD_TRUCK -> presenter.addTruck(
                    Truck(
                        comment.text.toString(),
                        null,
                        name.text.toString(),
                        price.text.toString())
                )

                ACTION_TYPE_EDIT_TRUCK -> presenter.editTruck(truckId,
                    Truck(
                        comment.text.toString(),
                        truckId,
                        name.text.toString(),
                        price.text.toString()))

            }
        }

        name.doOnTextChanged { text, start, count, after ->  showNameFieldValidation(after == 0) }
        price.doOnTextChanged { text, start, count, after ->  showPriceFieldValidation(after == 0) }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun initView(actionType: Int, truckName: String?, truckPrice: String?, truckComment: String?) {
        if (actionType == 1) {
            name.setText(truckName)
            price.setText(truckPrice)
            comment.setText(truckComment)
        }
    }

    override fun onSuccess() {
        Toast.makeText(this, getString(R.string.done), Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun onError(error: String) {
        Snackbar.make(findViewById(android.R.id.content), error, Snackbar.LENGTH_SHORT).show()
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun showNameFieldValidation(empty: Boolean) {
        if (empty) {
            nameInputLayout.error = "Поле не заполнено"
        } else {
            nameInputLayout.error = null
        }
    }

    override fun showPriceFieldValidation(empty: Boolean) {
        if (empty) {
            priceInputLayout.error = "Поле не заполнено"
        } else {
            priceInputLayout.error = null
        }
    }
}