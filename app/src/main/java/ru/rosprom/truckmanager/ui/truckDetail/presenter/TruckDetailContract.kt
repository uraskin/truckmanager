package ru.rosprom.truckmanager.ui.truckDetail.presenter

import ru.rosprom.truckmanager.data.jsonResponse.Truck


interface TruckDetailContract {
    fun addTruck(truck: Truck)

    fun editTruck(id: String, truck: Truck)
}