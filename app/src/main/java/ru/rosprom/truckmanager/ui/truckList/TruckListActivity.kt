package ru.rosprom.truckmanager.ui.truckList

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_truck_list.*
import ru.rosprom.truckmanager.R
import ru.rosprom.truckmanager.adapter.TrucksAdapter
import ru.rosprom.truckmanager.data.jsonResponse.Truck
import ru.rosprom.truckmanager.mvp.MvpAndroidxActivity
import ru.rosprom.truckmanager.ui.truckDetail.TruckDetailActivity
import ru.rosprom.truckmanager.ui.truckDetail.TruckDetailActivity.Companion.ACTION_TYPE_ADD_TRUCK
import ru.rosprom.truckmanager.ui.truckDetail.TruckDetailActivity.Companion.ACTION_TYPE_EDIT_TRUCK
import ru.rosprom.truckmanager.ui.truckList.presenter.TruckListPresenter
import ru.rosprom.truckmanager.ui.truckList.presenter.TruckListView


class TruckListActivity : MvpAndroidxActivity(), TruckListView, TrucksAdapter.OnTruckItemClickListener {

    @InjectPresenter
    lateinit var presenter: TruckListPresenter

    private var adapter: TrucksAdapter = TrucksAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_truck_list)

        setSupportActionBar(toolbarLayout as Toolbar)
        supportActionBar?.title = getString(R.string.truck_list)

        truckList.adapter = adapter
        truckList.layoutManager = LinearLayoutManager(this)

        addTruck.setOnClickListener {
            presenter.addNewTruck()
        }

        refreshLayout.setOnRefreshListener { presenter.loadTruckList() }
    }

    override fun onResume() {
        super.onResume()
        presenter.loadTruckList()
    }

    override fun showTruckList(trucks: List<Truck>?) {
        adapter.items = trucks
    }

    override fun showNewTruckScreen() {
        val i = Intent(this, TruckDetailActivity::class.java)
        i.putExtra(TruckDetailActivity.ACTION_TYPE, ACTION_TYPE_ADD_TRUCK)
        startActivity(i)
    }

    override fun showEditTruckScreen(truck: Truck) {
        val i = Intent(this, TruckDetailActivity::class.java)
        i.putExtra(TruckDetailActivity.ACTION_TYPE, ACTION_TYPE_EDIT_TRUCK)
        i.putExtra(TruckDetailActivity.TRUCK_ID, truck.id)
        i.putExtra(TruckDetailActivity.TRUCK_NAME, truck.nameTruck)
        i.putExtra(TruckDetailActivity.TRUCK_PRICE, truck.price)
        i.putExtra(TruckDetailActivity.TRUCK_COMMENT, truck.comment)

        startActivity(i)
    }

    override fun onTruckRemoved(truck: Truck) {
        presenter.loadTruckList()
        Toast.makeText(this, getString(R.string.truck_removed), Toast.LENGTH_SHORT).show()
    }

    override fun showProgressBar() {
        refreshLayout.isRefreshing = true
    }

    override fun hideProgressBar() {
        refreshLayout.isRefreshing = false
    }

    override fun showErrorMessage(error: String) {
        Snackbar.make(findViewById(android.R.id.content), error, Snackbar.LENGTH_SHORT).show()
    }

    override fun onItemClicked(truck: Truck) {
        showEditTruckScreen(truck)
    }

    override fun onRemoveButtonClicked(truck: Truck) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(getString(R.string.remove_truck_question))

        builder.setPositiveButton(getString(R.string.Yes)) { dialogInterface, i -> presenter.removeTruck(truck) }
        builder.setNegativeButton(getString(R.string.Cancel)) { dialogInterface, i -> dialogInterface.dismiss() }

        val dialog = builder.create()
        dialog.show()
    }
}