package ru.rosprom.truckmanager.ui.truckDetail.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.RequestBody
import ru.rosprom.truckmanager.App
import ru.rosprom.truckmanager.Utils
import ru.rosprom.truckmanager.api.MapNetworkCompletableErrors
import ru.rosprom.truckmanager.data.jsonResponse.Truck

@InjectViewState
class TruckDetailPresenter : MvpPresenter<TruckDetailView>(), TruckDetailContract {
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun addTruck(truck: Truck) {
        val validated = validateData(truck)

        if (validated) {

            viewState.showProgressBar()

            val jsonPost = Utils.getGson().toJson(truck)
            val body = RequestBody.create(MediaType.parse("application/octet-stream"), jsonPost)

            compositeDisposable.add(
                App.api.addTruck(body)
                    .compose(MapNetworkCompletableErrors())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        {
                            viewState.hideProgressBar()
                            viewState.onSuccess()
                        },
                        { error ->
                            viewState.hideProgressBar()
                            viewState.onError(error.localizedMessage)
                        })
            )
        }
    }

    override fun editTruck(id: String, truck: Truck) {
        val validated = validateData(truck)

        if (validated) {

            viewState.showProgressBar()

            val jsonPost = Utils.getGson().toJson(truck)
            val body = RequestBody.create(MediaType.parse("application/octet-stream"), jsonPost)

            compositeDisposable.add(
                App.api.editTruck(id, body)
                    .compose(MapNetworkCompletableErrors())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        {
                            viewState.hideProgressBar()
                            viewState.onSuccess()
                        },
                        { error ->
                            viewState.hideProgressBar()
                            viewState.onError(error.localizedMessage)
                        })
            )
        }
    }

    fun validateData(truck: Truck) : Boolean {
        var result = true

        if (truck.nameTruck!!.isEmpty()) {
            viewState.showNameFieldValidation(true)
            result = false
        }

        if (truck.price.isEmpty()) {
            viewState.showPriceFieldValidation(true)
            result = false
        }

        return result
    }
}