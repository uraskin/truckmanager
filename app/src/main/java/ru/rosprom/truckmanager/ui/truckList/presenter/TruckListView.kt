package ru.rosprom.truckmanager.ui.truckList.presenter

import com.arellomobile.mvp.MvpView
import ru.rosprom.truckmanager.data.jsonResponse.Truck


interface TruckListView : MvpView {
    fun showTruckList(trucks: List<Truck>?)

    fun showNewTruckScreen()

    fun showEditTruckScreen(truck: Truck)

    fun onTruckRemoved(truck: Truck)

    fun showProgressBar()

    fun hideProgressBar()

    fun showErrorMessage(error: String)
}