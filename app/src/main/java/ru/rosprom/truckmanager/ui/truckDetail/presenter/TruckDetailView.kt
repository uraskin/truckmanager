package ru.rosprom.truckmanager.ui.truckDetail.presenter

import com.arellomobile.mvp.MvpView


interface TruckDetailView : MvpView {
    fun onSuccess()

    fun onError(error: String)

    fun showNameFieldValidation(empty: Boolean)

    fun showPriceFieldValidation(empty: Boolean)

    fun showProgressBar()

    fun hideProgressBar()
}