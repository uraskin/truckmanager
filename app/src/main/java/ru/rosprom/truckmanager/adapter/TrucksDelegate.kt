package ru.rosprom.truckmanager.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import kotlinx.android.synthetic.main.item_list_truck.view.*
import ru.rosprom.truckmanager.R
import ru.rosprom.truckmanager.data.DiffItem
import ru.rosprom.truckmanager.data.jsonResponse.Truck


class TrucksDelegate(private val clickListener: TrucksAdapter.OnTruckItemClickListener) : AbsListItemAdapterDelegate<Truck, DiffItem, TrucksDelegate.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_list_truck, parent, false))
    }

    override fun isForViewType(item: DiffItem, items: MutableList<DiffItem>, position: Int): Boolean {
        return item is Truck
    }

    override fun onBindViewHolder(item: Truck, holder: ViewHolder, payloads: MutableList<Any>) {
        holder.bind(item, clickListener)
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private val name: TextView = itemView.name
        private val price: TextView = itemView.price
        private val comment: TextView = itemView.comment
        private val deleteTruck: ImageView = itemView.deleteTruck

        fun bind(item: Truck, clickListener: TrucksAdapter.OnTruckItemClickListener) {
            itemView.setOnClickListener { clickListener.onItemClicked(item) }
            deleteTruck.setOnClickListener { clickListener.onRemoveButtonClicked(item) }
            name.text = item.nameTruck
            price.text = item.price
            comment.text = item.comment
        }
    }
}
