package ru.rosprom.truckmanager.adapter

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import ru.rosprom.truckmanager.data.DiffItem
import ru.rosprom.truckmanager.data.jsonResponse.Truck


class TrucksAdapter(clickListener: OnTruckItemClickListener) : AsyncListDifferDelegationAdapter<DiffItem>(diffCallback) {
    init {

        delegatesManager.addDelegate(TrucksDelegate(clickListener))
    }

    companion object {
        private val diffCallback = object: DiffUtil.ItemCallback<DiffItem>() {
            override fun areItemsTheSame(oldItem: DiffItem, newItem: DiffItem): Boolean {
                return oldItem.getObjectId() == newItem.getObjectId()
            }

            override fun areContentsTheSame(oldItem: DiffItem, newItem: DiffItem): Boolean {
                return oldItem.getObjectHash() == newItem.getObjectHash()
            }
        }
    }

    interface OnTruckItemClickListener {
        fun onItemClicked(truck: Truck)
        fun onRemoveButtonClicked(truck: Truck)
    }
}