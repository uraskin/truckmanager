package ru.rosprom.truckmanager.data


interface DiffItem {
    fun getObjectId(): String?

    fun getObjectHash(): Int
}