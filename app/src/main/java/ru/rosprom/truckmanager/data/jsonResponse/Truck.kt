package ru.rosprom.truckmanager.data.jsonResponse

import ru.rosprom.truckmanager.data.DiffItem


data class Truck (
    val comment: String?,
    val id: String?,
    val nameTruck: String?,
    val price: String
)

    : DiffItem {
    override fun getObjectId(): String? {
        return id
    }

    override fun getObjectHash(): Int {
        return hashCode()
    }
}