package ru.rosprom.truckmanager.api

import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.*
import ru.rosprom.truckmanager.data.jsonResponse.Truck


interface TrucksApi {
    @GET("trucks")
    fun getTrucks() : Single<List<Truck>>

    @POST("truck/add")
    fun addTruck(@Body requestBody: RequestBody) : Completable

    @PATCH("truck/{id}")
    fun editTruck(@Path("id") id: String, @Body requestBody: RequestBody) : Completable

    @DELETE("truck/{id}")
    fun deleteTruck(@Path("id") id: String?) : Completable
}