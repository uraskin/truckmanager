package ru.rosprom.truckmanager.api


enum class TrucksApiErrors(val message: String) {
    NO_INTERNET_CONNECTION("Проверьте свое подключение к интернету"),
    TIMEOUT("Сервер не отвечает"),
    SERVER_ERROR("Произошла ошибка на сервере"),
    JSON_WRONG_FORMAT("Неверный формат данных")
}