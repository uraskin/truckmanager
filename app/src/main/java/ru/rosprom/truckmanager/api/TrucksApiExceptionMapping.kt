package ru.rosprom.truckmanager.api

import io.reactivex.*
import retrofit2.HttpException
import java.lang.reflect.InvocationTargetException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class MapNetworkSingleErrors<R> : SingleTransformer<R, R> {
    override fun apply(upstream: Single<R>): SingleSource<R> {
        return upstream.onErrorResumeNext {
                error -> when (error) {
            is SocketTimeoutException -> Single.error(NoNetworkException(TrucksApiErrors.TIMEOUT.message, error))
            is UnknownHostException -> Single.error(ServerUnreachableException(TrucksApiErrors.NO_INTERNET_CONNECTION.message, error))
            is HttpException -> Single.error(HttpCallFailureException(TrucksApiErrors.SERVER_ERROR.message, error))
            is InvocationTargetException -> Single.error(JsonParamNotFound(TrucksApiErrors.JSON_WRONG_FORMAT.message, error))
            else -> Single.error(error)
        }
        }
    }
}

class MapNetworkCompletableErrors : CompletableTransformer {
    override fun apply(upstream: Completable): CompletableSource {
        return upstream.onErrorResumeNext { error ->
            when (error) {
                is SocketTimeoutException -> Completable.error(NoNetworkException(TrucksApiErrors.TIMEOUT.message, error))
                is UnknownHostException -> Completable.error(ServerUnreachableException(TrucksApiErrors.NO_INTERNET_CONNECTION.message, error))
                is HttpException -> Completable.error(HttpCallFailureException(TrucksApiErrors.SERVER_ERROR.message, error))
                else -> Completable.error(error)
            }
        }}}
