package ru.rosprom.truckmanager.api


open class NetworkException(message: String, error: Throwable): RuntimeException(message, error)

class NoNetworkException(message: String, error: Throwable): NetworkException(message, error)

class ServerUnreachableException(message: String, error: Throwable): NetworkException(message, error)

class HttpCallFailureException(message: String, error: Throwable): NetworkException(message, error)


open class ReadDataException (message: String, error: Throwable) : RuntimeException(message, error)

class JsonParamNotFound(message: String, error: Throwable): ReadDataException(message, error)