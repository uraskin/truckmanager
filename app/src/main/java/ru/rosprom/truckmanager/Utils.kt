package ru.rosprom.truckmanager

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import ru.rosprom.truckmanager.data.jsonResponse.Truck
import java.lang.reflect.Type


class Utils {
    companion object {
        fun getGson() : Gson {
            return GsonBuilder()
                //.setExclusionStrategies()
                .registerTypeAdapter(
                    Truck::class.java, ConstructorInvocationDeserializer<Truck>(arrayOf("comment", "id", "nameTruck", "price")))
                .setPrettyPrinting()
                .setLenient()
                .create()
        }

        fun hideKeyboard(activity: Activity) {
            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = activity.currentFocus
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}

class ConstructorInvocationDeserializer<T>(private val fields: Array<String>) : JsonDeserializer<T> {
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): T {
        val clazz = TypeToken.get(typeOfT).rawType
        var args: Array<Any?> = arrayOf()
        val appropriateConstructor = clazz.constructors
            .filter { it.parameterTypes.size == fields.size }
            .find {
                try {
                    args = deserializeEach(json.asJsonObject, it.parameterTypes, context)
                    true
                } catch (e: JsonSyntaxException) {
                    false
                }
            }
        return appropriateConstructor!!.newInstance(*args) as T
    }

    @Throws(JsonSyntaxException::class)
    private fun deserializeEach(json: JsonObject, fieldTypes: Array<Class<*>>, context: JsonDeserializationContext): Array<Any?> {
        val result = Array<Any?>(fields.size, {null})
        fields.forEachIndexed { index, name ->
            result[index] = context.deserialize(json.get(name), fieldTypes[index])
        }
        return result
    }
}